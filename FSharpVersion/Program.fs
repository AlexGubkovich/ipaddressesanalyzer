﻿namespace FSharpVersion

module Program =
    let exitCode = 0

    [<EntryPoint>]
    let main args =
        try
            let settings = AppSettings.getAppSettings args failwith
            let logs = LogParser.parseLinesAsync settings.FileLog

            task {
                let! results = logs |> Analyzer.processLogsAsync settings

                do! ResultSaver.saveResultAsync settings.FileOutput results
            }
            |> Async.AwaitTask
            |> Async.RunSynchronously

        with ex ->
            printfn $"An error occurred: {ex.Message}"

        exitCode
