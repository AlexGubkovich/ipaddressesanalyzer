﻿namespace FSharpVersion

open System
open System.Net
open System.IO
open FSharpPlus
open Symbolica.Extensions.Configuration.FSharp
open Microsoft.Extensions.Configuration
open FsToolkit.ErrorHandling
open Helper

module Configuration =

    let private switchMappings =
        [ "--file-log", "FileLog"
          "--file-output", "FileOutput"
          "--address-start", "AddressStart"
          "--address-mask", "AddressMask"
          "--time-start", "TimeStart"
          "--time-end", "TimeEnd" ]
        |> Map.ofList

    let setpuConfig args =
        ConfigurationBuilder()
            .SetBasePath(Environment.CurrentDirectory)
            .AddJsonFile("appsettings.json", true)
            .AddCommandLine(args, switchMappings)
            .Build()

type AppSettings =
    { FileLog: string
      FileOutput: string
      AddressStart: IPAddress option
      AddressMask: IPAddress option
      TimeStart: DateOnly
      TimeEnd: DateOnly }

module AppSettings =

    let private bindIPAddress =
        Binder(fun (s: string) ->
            match tryParse<IPAddress> s with
            | Some ipAddress -> Success ipAddress
            | None -> Failure(ValueError.Message "IP address is not correct."))

    let private bindDate =
        Binder(fun (s: string) ->
            match DateOnly.TryParse(s) with
            | true, date -> Success date
            | false, _ -> Failure(ValueError.InvalidType "Date is not correct."))


    let private bindConfig =
        Bind.allOf
            [ Builder.bind {
                  let! fileLog = Bind.valueAt "FileLog" Bind.string
                  let! fileOutput = Bind.valueAt "FileOutput" Bind.string
                  let! addressStart = Bind.optValueAt "AddressStart" bindIPAddress
                  let! addressMask = Bind.optValueAt "AddressMask" bindIPAddress
                  let! timeStart = Bind.valueAt "TimeStart" bindDate
                  let! timeEnd = Bind.valueAt "TimeEnd" bindDate

                  return
                      { FileLog = fileLog
                        FileOutput = fileOutput
                        AddressStart = addressStart
                        AddressMask = addressMask
                        TimeStart = timeStart
                        TimeEnd = timeEnd }
              } ]

    let private bindAppSettings args =
        bindConfig
        |> Binder.eval (Configuration.setpuConfig args)
        |> BindResult.mapFailure (_.ToString())
        |> BindResult.map _.Head
        |> function 
            | BindResult.Success settings -> Ok settings
            | Failure err -> Error err

    let private validateSettings settings = 
        result {
            if not (Path.Exists(settings.FileLog)) then 
                return! Error $"File log with path {settings.FileLog} doesn't exist."

            if not (Path.Exists(settings.FileOutput)) then 
                return! Error $"File output with path {settings.FileOutput} doesn't exist."

            if settings.TimeEnd < settings.TimeStart then
                return! Error "TimeEnd не может быть меньше TimeStart."
            
            match settings.AddressStart, settings.AddressMask with
            | None, Some _ -> return! Error "AddressMask может быть задан только вместе с AddressStart."
            | Some start, Some mask when ipAddressToNum start > ipAddressToNum mask ->
                return! Error "Диапазон адресов задан не верно. AddressStart не может быть больше AddressMask."
            | _ -> ()

            return settings
        }

    let getAppSettings args errorHandler =
        bindAppSettings args
        |> Result.bind validateSettings
        |> function 
           | Ok s -> s
           | Error error -> errorHandler error
