﻿module ResultSaver

open Analyzer
open System.IO

let saveResultAsync (fileOutput: string) (outputEntries: OutputEntry array) =
    task {
        do! File.WriteAllTextAsync(fileOutput, "")

        use sw = new StreamWriter(fileOutput)

        for entry in outputEntries do
            let line = $"{entry.IPAddress}: {entry.Count}"
            do! sw.WriteLineAsync line
    }