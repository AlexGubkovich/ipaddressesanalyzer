﻿module Helper

open System.Net

let private rates = [| 256.0 ** 3; 256.0 ** 2; 256; 1 |]

let ipAddressToNum (address: IPAddress) =
    let bytes = address.GetAddressBytes()

    Array.map2 (fun byte rate -> float byte * rate) bytes rates |> Array.sum
