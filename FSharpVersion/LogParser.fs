﻿module LogParser

open System
open System.Net
open System.IO
open FsToolkit.ErrorHandling
open FSharp.Control
open System.Runtime.CompilerServices

[<Struct; IsReadOnly>]
type LogEntry =
    { IPAddress: IPAddress; Date: DateOnly }

let private parseIPAddress (address: string) =
    match IPAddress.TryParse(address) with
    | true, ip -> Some ip
    | false, _ -> None

let private parseDate (dateStr: string) =
    match DateOnly.TryParse(dateStr) with
    | true, date -> Some date
    | false, _ -> None

let private parseLine (line: string) lineNumber =
    let parts = line.Split([| ':' |], 2)

    if parts.Length <> 2 then
        Error $"{lineNumber} line is not valid"
    else
        result {
            let! ipAddress = 
                match parseIPAddress parts[0] with
                | Some ip -> Ok ip
                | None -> Error $"Address in {lineNumber} line is not valid"

            let! date = 
                parts[1].Split(' ') 
                |> Array.head
                |> parseDate
                |> function 
                    | Some date -> Ok date
                    | None -> Error $"Date in {lineNumber} line is not valid"

            return { IPAddress = ipAddress; Date = date }
        }

let parseLinesAsync (filePath: string) =
    taskSeq {
        use reader = new StreamReader(filePath)
        let mutable lineNumber = 1

        while not reader.EndOfStream do
            let! line = reader.ReadLineAsync()
            let parseResult = parseLine line lineNumber

            match parseResult with
            | Ok logEntry ->
                lineNumber <- lineNumber + 1
                logEntry
            | Error err -> failwith err
    }