﻿module Analyzer

open Helper
open LogParser
open System.Net
open FSharpVersion
open FSharp.Control
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks

type OutputEntry = { IPAddress: IPAddress; Count: int64 }

let private createIsInRange addressStart addressMask =

    let addressStartNum = addressStart |> Option.map ipAddressToNum

    let addressMaskNum = addressMask |> Option.map ipAddressToNum

    fun address ->
        let addressNum = ipAddressToNum address

        let addressStartCheck =
            addressStartNum |> Option.forall (fun startNum -> addressNum >= startNum)

        let addressMaskCheck =
            addressMaskNum |> Option.forall (fun maskNum -> addressNum <= maskNum)

        addressStartCheck && addressMaskCheck

let processLogsAsync (settings: AppSettings) (entries: IAsyncEnumerable<LogEntry>) =
    let isInRange = createIsInRange settings.AddressStart settings.AddressMask

    task {
        let! results =
            entries
                .Where(fun entry ->
                    entry.Date >= settings.TimeStart
                    && entry.Date <= settings.TimeEnd
                    && isInRange entry.IPAddress)
                .GroupBy(fun x -> x.IPAddress)
                .Select(fun x ->
                    task {
                        let! count = x.CountAsync()
                        return { IPAddress = x.Key; Count = count }
                    })
                .ToListAsync()

        return! Task.WhenAll(results)
    }
