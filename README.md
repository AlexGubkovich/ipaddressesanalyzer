# IPAddressesAnalyzer



## Getting started

Сразу стоит отметить что приоретет конфигурации отдается файлу appsettings.json

Вот его пример

```json
{
  "FileLog": "C:\\Users\\alexg\\OneDrive\\Рабочий стол\\logs.txt",
  "FileOutput": "C:\\Users\\alexg\\OneDrive\\Рабочий стол\\output.txt",
  "AddressStart": "172.16.1.1",
  "AddressMask": "190.16.1.1",
  "TimeStart": "10.04.2024",
  "TimeEnd": "12.05.2024"
}
```

Пример команды запуска c указанием всех параметров

```console
IPAddressesAnalyzer.exe --file-log="C:\\Users\\alexg\\OneDrive\\Рабочий стол\\logs.txt" --file-output="C:\\Users\\alexg\\OneDrive\\Рабочий стол\\output.txt" --time-start="10.04.2024" --time-end="12.05.2024" --address-start="172.16.1.1" --address-mask="190.16.1.1"
```

#### Пример логов

180.0.0.0:2024-04-9 08:15:30

180.0.0.0:2024-04-10 08:15:30

180.0.0.0:2024-05-12 08:15:30

180.0.0.0:2024-06-12 08:15:30

172.16.1.1:2024-04-10 08:15:30

172.16.1.0:2024-04-10 08:15:30

190.16.1.1:2024-04-10 08:15:30

190.15.0.1:2024-04-14 00:00:00

190.15.0.1:2024-04-14 00:00:00

190.17.1.1:2024-04-10 08:15:30

#### Ожидаемый вывод для вышеприведенных логов и параметров

180.0.0.0: 2

172.16.1.1: 1

190.16.1.1: 1

190.15.0.1: 2
