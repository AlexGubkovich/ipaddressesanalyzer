﻿using IPAddressesAnalyzer.Configuration;
using IPAddressesAnalyzer.Services;
using Microsoft.Extensions.DependencyInjection;

namespace IPAddressesAnalyzer;

public class Program
{
    public static async Task Main(string[] args)
    {
        try
        {
            var serviceProvider = RegisterServices(args);

            var logParser = serviceProvider.GetRequiredService<LogParser>();
            var analyzer = serviceProvider.GetRequiredService<Analyzer>();
            var resultSaver = serviceProvider.GetRequiredService<ResultSaver>();

            var logEntries = logParser.ParseLinesAsync();
            var outputs = await analyzer.ProcessLogsAsync(logEntries);

            await resultSaver.SaveResultsAsync(outputs);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Ошибка: {ex.Message}");
        }
    }


    private static ServiceProvider RegisterServices(string[] args)
    {
        var configuration = ConfigurationService.SetupConfiguration(args);

        return new ServiceCollection()
            .AddSingleton(configuration.GetOptions())
            .AddSingleton<LogParser>()
            .AddSingleton<Analyzer>()
            .AddSingleton<ResultSaver>()
            .BuildServiceProvider();
    }
}
