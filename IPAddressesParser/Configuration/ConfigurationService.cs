﻿using Microsoft.Extensions.Configuration;

namespace IPAddressesAnalyzer.Configuration;

internal static class ConfigurationService
{
    public static IConfiguration SetupConfiguration(string[] args)
    {
        var switchMappings = new Dictionary<string, string>()
           {
               { "--file-log", "FileLog" },
               { "--file-output", "FileOutput" },
               { "--address-start", "AddressStart" },
               { "--address-mask", "AddressMask" },
               { "--time-start", "TimeStart" },
               { "--time-end", "TimeEnd" },
           };

        return new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", true)
            .AddCommandLine(args, switchMappings)
            .Build();
    }


    public static AppOptions GetOptions(this IConfiguration configuration)
    {
        var options = configuration.Get<AppOptions>() ?? throw new Exception("Не удалось получить параметры");

        ValidateOptions(options);

        return options;
    }

    private static void ValidateOptions(AppOptions options)
    {
        ValidateRequired(options.FileLog, nameof(AppOptions.FileLog));
        ValidateRequired(options.FileOutput, nameof(AppOptions.FileOutput));
        ValidateRequired(options.TimeStart, nameof(AppOptions.TimeStart));
        ValidateRequired(options.TimeEnd, nameof(AppOptions.TimeEnd));

        if (string.IsNullOrEmpty(options.AddressStart) && !string.IsNullOrEmpty(options.AddressMask))
        {
            throw new Exception("AddressMask может быть задан только с AddressStart");
        }
    }

    private static void ValidateRequired(string value, string paramName)
    {
        if (string.IsNullOrEmpty(value))
        {
            throw new ArgumentNullException(paramName, $"{paramName} cannot be null or empty.");
        }
    }
}
