﻿namespace IPAddressesAnalyzer.Configuration;

internal class AppOptions
{
    public required string FileLog { get; set; }

    public required string FileOutput { get; set; }

    public string? AddressStart { get; set; }

    public string? AddressMask { get; set; }

    public required string TimeStart { get; set; }

    public required string TimeEnd { get; set; }
}
