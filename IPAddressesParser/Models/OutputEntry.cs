﻿using System.Net;

namespace IPAddressesAnalyzer.Models;

internal record OutputEntry(IPAddress Address, int Count);