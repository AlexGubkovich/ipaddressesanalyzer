﻿using System.Net;

namespace IPAddressesAnalyzer.Models;

internal record LogEntry(IPAddress Address, DateOnly Date);
