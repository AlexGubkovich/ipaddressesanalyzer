﻿using IPAddressesAnalyzer.Configuration;
using IPAddressesAnalyzer.Models;

namespace IPAddressesAnalyzer.Services;

internal class ResultSaver
{
    private readonly string fileOutput;

    public ResultSaver(AppOptions options)
    {
        fileOutput = options.FileOutput;

        ValidateOutputPath();
    }

    public async Task SaveResultsAsync(IEnumerable<OutputEntry> outputEntries)
    {
        await File.WriteAllTextAsync(fileOutput, "");

        using var streamWriter = new StreamWriter(fileOutput);

        foreach (var entry in outputEntries)
        {
            await streamWriter.WriteLineAsync($"{entry.Address}: {entry.Count}");
        }
    }

    private void ValidateOutputPath()
    {
        var outputDirectoryPath = Path.GetDirectoryName(fileOutput);

        if (!Path.Exists(outputDirectoryPath) && !Path.Exists(fileOutput))
        {
            throw new ArgumentException($"Output file {fileOutput} or its directory doesn't exist.");
        }
    }
}
