﻿using IPAddressesAnalyzer.Configuration;
using IPAddressesAnalyzer.Models;
using System.Net;

namespace IPAddressesAnalyzer.Services;

internal class LogParser
{
    private readonly string fileLog;

    public LogParser(AppOptions options)
    {
        fileLog = options.FileLog;

        if (!Path.Exists(fileLog))
        {
            throw new ArgumentException($"File log with path {fileLog} doesn't exist.");
        }
    }


    public async IAsyncEnumerable<LogEntry> ParseLinesAsync()
    {
        var index = 1;

        using StreamReader reader = File.OpenText(fileLog);

        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();

            var splitedLine = line!.Split(":", 2);

            if (splitedLine.Length != 2)
                throw new InvalidDataException($"{index} line is not valid");

            if (!IPAddress.TryParse(splitedLine[0], out var address))
                throw new InvalidDataException($"Address in {index} line is not valid");

            if (!DateOnly.TryParse(splitedLine[1].Split(" ").FirstOrDefault(), out var date))
                throw new InvalidDataException($"Date in {index} line is not valid");

            index++;

            yield return new LogEntry(address, date);
        }
    }
}
