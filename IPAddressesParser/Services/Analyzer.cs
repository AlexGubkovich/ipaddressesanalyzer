﻿using IPAddressesAnalyzer.Configuration;
using IPAddressesAnalyzer.Models;
using System.Net;

namespace IPAddressesAnalyzer.Services;

internal class Analyzer
{
    private readonly double? addressStartNum;

    private readonly double? addressMaskNum;

    private readonly DateOnly timeStart;

    private readonly DateOnly timeEnd;

    public Analyzer(AppOptions options)
    {
        ParseIPAddress(options.AddressStart, ref addressStartNum, nameof(AppOptions.AddressStart));
        ParseIPAddress(options.AddressMask, ref addressMaskNum, nameof(AppOptions.AddressMask));

        ParseDate(options.TimeStart, ref timeStart, nameof(AppOptions.TimeStart));
        ParseDate(options.TimeEnd, ref timeEnd, nameof(AppOptions.TimeEnd));

        ValidateParams();
    }

    public async Task<OutputEntry[]> ProcessLogsAsync(IAsyncEnumerable<LogEntry> entries)
    {
        var results = await entries
            .Where(x => x.Date >= timeStart && x.Date <= timeEnd && IsInRange(x.Address))
            .GroupBy(x => x.Address)
            .Select(async x => new OutputEntry(x.Key, await x.CountAsync()))
            .ToListAsync();

        return await Task.WhenAll(results);
    }


    private bool IsInRange(IPAddress address)
    {
        var addressNum = IDAddressToNum(address);

        if (addressStartNum != null && addressNum < addressStartNum)
            return false;

        if (addressMaskNum != null && addressNum > addressMaskNum)
            return false;

        return true;
    }

    private static double IDAddressToNum(IPAddress address)
    {
        byte[] ipBytes = address.GetAddressBytes();
        return ipBytes[0] * Math.Pow(256, 3) + ipBytes[1] * Math.Pow(256, 2) + ipBytes[2] * 256 + ipBytes[3];
    }

    private static void ParseIPAddress(string? value, ref double? outputaIPNum, string paramName)
    {
        if (string.IsNullOrEmpty(value)) return;

        if (!IPAddress.TryParse(value, out var outputAddress))
        {
            throw new ArgumentException($"{paramName} param is not valid.");
        }

        outputaIPNum = IDAddressToNum(outputAddress);
    }

    private static void ParseDate(string? value, ref DateOnly output, string paramName)
    {
        if (!DateOnly.TryParse(value, out output))
        {
            throw new ArgumentException($"{paramName} date is not valid.");
        }
    }

    private void ValidateParams()
    {
        ValidateDates();
        ValidateAddressRanges();
    }

    private void ValidateAddressRanges()
    {
        if (addressMaskNum == null || addressStartNum == null) return;

        if (addressStartNum > addressMaskNum)
            throw new ArgumentException("Диапазон адресов задан не верно. AddressStart не может быть больше AddressMask.");
    }

    private void ValidateDates()
    {
        if (timeEnd < timeStart)
            throw new ArgumentException("TimeEnd не может быть меньше TimeStart.");
    }
}
