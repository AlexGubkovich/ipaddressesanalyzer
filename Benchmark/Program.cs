﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace Benchmark
{
    [MemoryDiagnoser(true)]
    public class MyBenchmark
    {
        [Benchmark]
        public void RunConsoleApp1()
        {
            IPAddressesAnalyzer.Program.Main([]).Wait();
        }

        [Benchmark]
        public void RunConsoleApp2()
        {
            FSharpVersion.Program.main([]);
        }
    }

    internal class Program
    {   
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<MyBenchmark>();
        }
    }
}
